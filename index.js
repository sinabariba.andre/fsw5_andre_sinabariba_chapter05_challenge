// Imports
const express = require('express');
const fs = require('fs');
const morgan = require('morgan');
const bodyParser= require('body-parser');
const app= express();
const router = require('./routes/route');

const port = 3000;


// Middleware
app.use(express.json());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));


// Set views
app.set('view engine','ejs');

// Static File
app.use(express.static('public'))
app.use('/css', express.static(__dirname + '/public/assets/css'))
app.use('/js', express.static(__dirname + '/public/js'))
app.use('/img', express.static(__dirname + '/public/assets/img'))


// Set endpoint
app.use('/',router);


app.listen(port, ()=>{
    console.log(`Server Listening on port ${port}`);
})
